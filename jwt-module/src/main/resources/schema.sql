CREATE TABLE api_user
( username VARCHAR(25) NOT NULL
, bcrypt_hash VARCHAR(254)
, primary key (username)
);
