package com.mgsoft.jwtmodule.config;

import com.mgsoft.jwtmodule.model.JwtClaims;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;


public class JwtSignatureAuthToken extends AbstractAuthenticationToken {

	private static final long serialVersionUID = 2342023340781234L;

	private String jwt64;
	private JwtClaims jwtClaims;

	public JwtSignatureAuthToken(String token, JwtClaims jwtClaims){
		super(AuthorityUtils.NO_AUTHORITIES);
		this.jwt64 = token;
		this.jwtClaims = jwtClaims;
	}

	public JwtClaims getJwtClaims() {
		return jwtClaims;
	}

	public String getJwt() {
		return jwt64;
	}

	// abstract methods, could be done better :
	@Override
	public Object getCredentials() {
		return jwt64;
	}

	@Override
	public Object getPrincipal() {
		return jwt64;
	}

}
