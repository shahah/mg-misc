package com.mgsoft.jwtmodule.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
// import org.springframework.security.web.util.matcher.AndRequestMatcher;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.AnyRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.context.annotation.RequestScope;
import com.mgsoft.jwtmodule.model.JwtClaims;


// https://docs.spring.io/spring-security/site/docs/current/reference/html5/#multiple-httpsecurity
// @Configuration // superfluous , implied by @EnableWebSecurity
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {


	// put the backend under this prefix :
	private static final RequestMatcher JWT_PROTECTED_URLS =
		new AntPathRequestMatcher("/secure/**");

	private static final RequestMatcher POST_REQUEST_JWT_PROTECTED_URLS =
		new AntPathRequestMatcher("/post-secure/**");

	//private static final RequestMatcher AUTH_URL =
	//	new OrRequestMatcher(new AntPathRequestMatcher("/authenticate"));

	// the following bean can be used to pass username from auth filter to controllers
	// simply @Autowire it
	@Bean
	@RequestScope
	public JwtClaims jwtData() {
		return new JwtClaims();
	}


	@Configuration
	@Order(1)
	public static class NoAuthConfig extends WebSecurityConfigurerAdapter {

		// does this need a declared auth manager ?

		@Bean(name = "freeAM")
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			httpSecurity
				.csrf().disable() // no cookies
				.authorizeRequests()
				////.requestMatchers(AUTH_URL).permitAll()
				.requestMatchers (
					new NegatedRequestMatcher(
						new OrRequestMatcher(JWT_PROTECTED_URLS,POST_REQUEST_JWT_PROTECTED_URLS))).permitAll()
				//.antMatchers("/authenticate").permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		}
	}




	@Configuration // no @Order means LAST
	public static class JwtAuthConfig extends WebSecurityConfigurerAdapter {


		@Autowired
		private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

		@Primary
		@Bean(name = "jwtAM")
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

		@Bean
		public AuthenticationFailureHandler failureHandler() {
			return new SimpleUrlAuthenticationFailureHandler("/index.html");
		}

		@Bean
		JwtAuthFilter jwtAuthFilter() throws Exception {
			final JwtAuthFilter filter = new JwtAuthFilter(JWT_PROTECTED_URLS);
			filter.setAuthenticationManager(authenticationManagerBean());
			// UNUSED :
			// filter.setAuthenticationFailureHandler(failureHandler());

			// filter.setAuthenticationSuccessHandler(successHandler()); // ??
			return filter;
		}


		@Bean
		PostRequestJwtAuthFilter postRequestJwtAuthFilter() throws Exception {
			final PostRequestJwtAuthFilter filter = new PostRequestJwtAuthFilter(POST_REQUEST_JWT_PROTECTED_URLS);
			filter.setAuthenticationManager(authenticationManagerBean());
			return filter;
		}
		// @Autowired
		// private JwtRequestFilter jwtRequestFilter;



		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			// We don't need CSRF for this example
			httpSecurity.csrf().disable().authorizeRequests()
				//.antMatchers("/**").authenticated()
				.requestMatchers(JWT_PROTECTED_URLS).authenticated()
				.anyRequest().authenticated()
				// make sure we use stateless session; session won't be used to
				// store user's state.
				.and()
				// no auth happens without these two lines :
				.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				// .and().addFilterBefore(jwtAuthFilter(), AnonymousAuthenticationFilter.class); // UsernamePasswordAuthenticationFilter.class) ;
				.and()
				.addFilterBefore(jwtAuthFilter(), AnonymousAuthenticationFilter.class) // UsernamePasswordAuthenticationFilter.class) ;
				.addFilterBefore(postRequestJwtAuthFilter(), AnonymousAuthenticationFilter.class);
			//.addFilterBefore(jwtRequestFilter, AnonymousAuthenticationFilter.class);
			// so security merely forces you to have an auth header
			// the filters do the auth checking ? (?)
		}

		@Autowired
		private JwtSignatureAuthProvider jwtSAP;

		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			// super.configure(auth); // ?
			auth.authenticationProvider(jwtSAP);
		}

	}
}
