package com.mgsoft.jwtmodule.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mgsoft.jwtmodule.model.JwtClaims;
import com.mgsoft.jwtmodule.util.JwtTokenUtilPublic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.util.matcher.RequestMatcher;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.security.SignatureException;


// @Component
public class JwtAuthFilter extends AbstractAuthenticationProcessingFilter {

	protected JwtAuthFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
		super(requiresAuthenticationRequestMatcher);
		//setAuthenticationManager(new NoOpAuthenticationManager()); // ?
		// Auto-generated constructor stub
	}

	@Autowired
	private JwtClaims jwtClaims; // bean def in WebSecurityConfig

	// @Override
	// public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
	// throws IOException, ServletException { ... }
	//
	// doFilter calls attemptAuth

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {

		final String requestTokenHeader = request.getHeader("Authorization");
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			String jwToken = requestTokenHeader.substring(7); // strip "Bearer "

			// may throw :
			Authentication auth = getAuthenticationManager()
				.authenticate(new JwtSignatureAuthToken(jwToken,jwtClaims));

			System.out.println("JWT affirmed by auth manager");
			return auth;
		}
		else {
			// caught in doFilter :
			System.out.println("Improper token");
		        throw new BadCredentialsException("JWT does not begin with Bearer String");
			// // runtime exception :
			// throw new IllegalArgumentException("JWT does not begin with Bearer String");
		}

	}

	@Override
	protected void successfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
			final FilterChain chain, final Authentication authResult) throws IOException, ServletException {
		SecurityContextHolder.getContext().setAuthentication(authResult);
		logger.info("Auth successful");
		// DO NOT : super.successfulAuthentication ()
		chain.doFilter(request, response);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		logger.info("a failed attempt at auth");
		super.unsuccessfulAuthentication(request, response, failed);
	}

}
