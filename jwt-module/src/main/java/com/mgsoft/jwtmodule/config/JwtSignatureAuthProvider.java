package com.mgsoft.jwtmodule.config;

import com.mgsoft.jwtmodule.model.JwtClaims;
import com.mgsoft.jwtmodule.util.JwtTokenUtilPublic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;

@Component
public class JwtSignatureAuthProvider implements AuthenticationProvider {

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(JwtSignatureAuthToken.class);
	}

	@Autowired
	private JwtTokenUtilPublic jwtTokenUtilPublic;

	@Override
	public Authentication authenticate(Authentication authentication)
		throws AuthenticationException {
		System.out.println("reached JwtSignatureAuthProvider");
		JwtSignatureAuthToken token = (JwtSignatureAuthToken) authentication;
		String jwToken = token.getJwt();

		try {
			// check the signature (may throw) :
			String username = jwtTokenUtilPublic.getUsernameFromToken(jwToken);

			// pass on to controllers :
			// QUESTION : is it nonsense for an AuthenticationProvider
			//   to write to such a bean
			token.getJwtClaims().setUsername(username);

			return token;

		} catch (IllegalArgumentException e) {
			throw new BadCredentialsException("JWT ineffable");
		} catch (ExpiredJwtException e) {
			throw new BadCredentialsException("JWT is no more");
			// throw new BadCredentialsException("JWT has ceased to be");
		} catch (SignatureException e) {
			throw new BadCredentialsException("JWT signature doesn't check out");
			// System.out.println("JWT signature doesn't check out");
		}
	}

}
