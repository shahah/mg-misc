package com.mgsoft.jwtmodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtModuleApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtModuleApplication.class, args);
	}

}
