package com.mgsoft.jwtmodule.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ApiUser implements Serializable {

	private static final long serialVersionUID = 2342360L;

	@Id
	@Column
	private String username ;

	@Column
	private String bcryptHash ;

	public String getUsername() {
		return username;
	}
	public String getBcryptHash() {
		return bcryptHash;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setBcryptHash(String bcryptHash) {
		this.bcryptHash = bcryptHash;
	}
}
