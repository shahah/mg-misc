package com.mgsoft.jwtmodule.model;

import java.io.Serializable;

public class JwtClaims implements Serializable {

	private static final long serialVersionUID = 2342888L;

	private String username ;
	// private String role ;

	public JwtClaims(){
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
