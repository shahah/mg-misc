package com.mgsoft.jwtmodule.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@CrossOrigin
//@RestController // = @Controller + @ResponseBody // you don't want that here
@Controller
@RequestMapping({ "/store-token" })
@PropertySource("classpath:values.properties")
public class TokenIntoSessionStorage {


	@PostMapping(produces = "text/html",
		     // consumes = "application/x-www-form-urlencoded")
		     consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	public String
	makeToken(@RequestParam Map<String,String> req,
		  Model model,
		  HttpServletResponse response,
		  @Value("${js.singlepageapp.path}") String redirect)
			throws Exception {
		String token = req.get("token");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		// response.addHeader("Cache-Control","no-cache"); // overriden by no-store
		model.addAttribute("token",token);
		model.addAttribute("redirect",redirect);

		return "store-token";
	}
}
