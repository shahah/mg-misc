package com.mgsoft.jwtmodule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
// import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

import com.mgsoft.jwtmodule.model.JwtClaims;

@CrossOrigin
@Controller
@RequestMapping(value = "/post-secure/print-params")
public class PostPrintParams {

	@Autowired
	JwtClaims jwtClaims;

	@PostMapping(produces = { MediaType.TEXT_HTML_VALUE }, consumes = {
			MediaType.APPLICATION_FORM_URLENCODED_VALUE })
	public String printRequestParams(@RequestParam Map<String, String> reqParams,
			//HttpServletResponse response,
			Model model) {
		model.addAttribute("reqParams", reqParams);
		return "print-params";
	}
}
