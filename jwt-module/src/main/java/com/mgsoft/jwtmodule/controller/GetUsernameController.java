package com.mgsoft.jwtmodule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.mgsoft.jwtmodule.model.JwtClaims;

@RestController
@RequestMapping(value = "/secure/which-user")
public class GetUsernameController {

	@Autowired
	JwtClaims jwtClaims;


	@RequestMapping (method=RequestMethod.GET)
	public String getAdmUser(){

		return jwtClaims.getUsername();
	}
}
