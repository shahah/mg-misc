package com.mgsoft.jwtmodule.spec ;

import java.security.PublicKey;

public interface PublicKeyed {
	public PublicKey getPublicKey();
}
