package com.mgsoft.jwtmodule.repository;

import com.mgsoft.jwtmodule.model.ApiUser;

import org.springframework.data.jpa.repository.JpaRepository;
// import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiUserRepository extends JpaRepository<ApiUser, String> {
    // should be generated by spring ? :
    // Optional<ApiUser> findOptionalByUsername(String username);
}
