package com.mgsoft.jwtauthserver.service;

import java.util.NoSuchElementException;
import java.util.Optional;

//import javax.transaction.Transactional;

import com.mgsoft.jwtauthserver.model.ApiUser;
import com.mgsoft.jwtauthserver.repository.ApiUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ApiUserService {

	@Autowired
	ApiUserRepository aur;

	@Autowired
	BCryptPasswordEncoder bcpEncoder;

	@Transactional
	public void updatePassword (String username, String newPassword) throws UsernameNotFoundException {
		Optional<ApiUser> optUser = aur.findById(username);
		try {
			ApiUser user = optUser.get();
			user.setBcryptHash(bcpEncoder.encode(newPassword));
			aur.save(user);
		}
		catch (NoSuchElementException e) {
			// nothing to update ?
		}


	}
}
