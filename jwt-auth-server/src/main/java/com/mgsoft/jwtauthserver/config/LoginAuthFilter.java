package com.mgsoft.jwtauthserver.config;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
// import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mgsoft.jwtauthserver.model.JwtClaims;
import com.mgsoft.jwtauthserver.model.LoginErrors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class LoginAuthFilter extends UsernamePasswordAuthenticationFilter {

	protected LoginAuthFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
		super () ;
		this.setRequiresAuthenticationRequestMatcher (requiresAuthenticationRequestMatcher) ;
		this.setUsernameParameter("username");
		this.setPasswordParameter("password");
	}


	@Autowired
	private JwtClaims jwtClaims ; // bean def in WebSecurityConfig

	@Autowired
	private LoginErrors loginErrors;

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		System.out.println("trying to auth");
		// auth according to UsernamePasswordAuthenticationFilter :
		return super.attemptAuthentication(request, response);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		// // dodge all redirecting SuccessHandlers, do not call super
		jwtClaims.setUsername(this.obtainUsername(request)) ;
		super.successfulAuthentication(request, response, chain, authResult);
		// SecurityContextHolder.getContext().setAuthentication(authResult);
		// chain.doFilter(request, response);
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
						  AuthenticationException failed) throws IOException, ServletException {
		loginErrors.getErrors().add("Incorrect username or password.");
		loginErrors.setUsername(Optional.ofNullable(obtainUsername(request)));
		super.unsuccessfulAuthentication(request, response, failed);
	}
}
