package com.mgsoft.jwtauthserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
// import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
// import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.ForwardAuthenticationFailureHandler;
import org.springframework.security.web.authentication.ForwardAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
// import org.springframework.security.web.authentication.AuthenticationFailureHandler;
// import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
// import org.springframework.security.web.authentication.ForwardAuthenticationSuccessHandler;
// import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
// import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
// import org.springframework.security.web.util.matcher.AndRequestMatcher;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
// import org.springframework.security.web.util.matcher.AndRequestMatcher;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
// import org.springframework.security.web.util.matcher.AnyRequestMatcher;
// import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.context.annotation.RequestScope;
import com.mgsoft.jwtauthserver.model.JwtClaims;
// import com.mgsoft.jwtauthserver.service.ApiUserDetailsService;
import com.mgsoft.jwtauthserver.model.LoginErrors;


// https://docs.spring.io/spring-security/site/docs/current/reference/html5/#multiple-httpsecurity
// @Configuration // superfluous , implied by @EnableWebSecurity
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {


	private static final RequestMatcher FILTERED_JWT =
		new OrRequestMatcher(new AntPathRequestMatcher("/welcome"),
				     new AntPathRequestMatcher("/login",HttpMethod.POST.name()));
	private static final RequestMatcher FILTERED_CHANGE_PASSWORD = new AntPathRequestMatcher("/change-password",HttpMethod.POST.name());
	private static final RequestMatcher UNAME_PASS_PROTECTED_URLS =
			new OrRequestMatcher(FILTERED_JWT, FILTERED_CHANGE_PASSWORD);


	@Bean
	@RequestScope
	public JwtClaims jwtClaims() {
		return new JwtClaims();
	}

	@Bean
	@RequestScope
	public LoginErrors loginErrors() {
		return new LoginErrors();
	}


	// @Configuration
	// @Order(1)
	// public static class NoAuthConfig extends WebSecurityConfigurerAdapter {

	// 	// does this need a declared auth manager ?
	// 	@Bean(name="noAuthAM")
	// 	@Override
	// 	public AuthenticationManager authenticationManagerBean() throws Exception {
	// 		return super.authenticationManagerBean();
	// 	}

	// 	@Override
	// 	protected void configure(HttpSecurity httpSecurity) throws Exception {
	// 		httpSecurity
	// 			.csrf().disable() // no cookies
	// 			.authorizeRequests()
	// 			////.requestMatchers(AUTH_URL).permitAll()
	// 			.requestMatchers(new NegatedRequestMatcher(UNAME_PASS_PROTECTED_URLS)).permitAll()
	// 			//.antMatchers("/authenticate").permitAll()
	// 			.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
	// 			.and()
	// 			.sessionManagement()
	// 			.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	// 	}
	// }

	@Configuration
	// @Order(2)
	public static class UsernamePasswordAuthConfig extends WebSecurityConfigurerAdapter {


		@Bean(name="unamePassAM")
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}


		@Bean(name="issueJwtFilter")
		LoginAuthFilter issueJwtFilter() throws Exception {
			final LoginAuthFilter filter = new LoginAuthFilter(FILTERED_JWT);
			filter.setAuthenticationManager(authenticationManager());
			filter.setAuthenticationFailureHandler(new ForwardAuthenticationFailureHandler("/login/error"));
			filter.setAuthenticationSuccessHandler(new ForwardAuthenticationSuccessHandler("/welcome"));
			//filter.setAuthenticationSuccessHandler(new ForwardAuthenticationSuccessHandler("/welcome"));
			//filter.setAuthenticationFailureHandler(new SimpleUrlAuthenticationFailureHandler("/login"));
			// SavedRequestAwareAuthenticationSuccessHandler ash = new SavedRequestAwareAuthenticationSuccessHandler() ;
			// // defaults to '/login' ? :
			// ash.setTargetUrlParameter("/welcome");
			// filter.setAuthenticationSuccessHandler(ash);
			return filter;
		}

		@Bean(name="changePasswordFilter")
		LoginAuthFilter changePasswordFilter() throws Exception {
			final LoginAuthFilter filter = new LoginAuthFilter(FILTERED_CHANGE_PASSWORD);
			filter.setAuthenticationFailureHandler(new ForwardAuthenticationFailureHandler("/change-password/error"));
			filter.setAuthenticationSuccessHandler(new ForwardAuthenticationSuccessHandler("/change-password"));
			filter.setAuthenticationManager(authenticationManager());
			return filter;
		}

		@Autowired
		private UserDetailsService uds ;

		// spring magic rewrites this method ?
		// so that it makes only one PasswordEncoder
		@Bean
		public PasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder(); // BCrypt hash
		}


		@Override
		protected void configure(AuthenticationManagerBuilder amb) throws Exception {
			amb.userDetailsService(uds).passwordEncoder(passwordEncoder());
		}

		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			// We don't need CSRF for this example
			httpSecurity
				.csrf().disable() // no cookies
				.authorizeRequests()
				////.requestMatchers(AUTH_URL).permitAll()
				.requestMatchers(UNAME_PASS_PROTECTED_URLS).authenticated()
				//.antMatchers("/authenticate").permitAll()
				.and()
				// probably responsible for clearing SecurityContextHolder after each request :
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.addFilterBefore(issueJwtFilter(), AnonymousAuthenticationFilter.class)
				.addFilterBefore(changePasswordFilter(), AnonymousAuthenticationFilter.class);

		}
	}
}
