package com.mgsoft.jwtauthserver.controller;

import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import com.mgsoft.jwtauthserver.util.JwtTokenUtilPrivate;
import com.mgsoft.jwtauthserver.model.ApiUser;
// import com.mgsoft.model.JwtRequest;
// import com.mgsoft.model.JwtResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


// import java.util.ArrayList;
// import java.util.List;

@CrossOrigin
//@RestController // = @Controller + @ResponseBody // you don't want that here
@Controller
@RequestMapping({ "/welcome" })
@PropertySource("classpath:values.properties")
public class RedirectWithJwt {

	// using auth manager outside filters
	@Autowired
	@Qualifier("unamePassAM")
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtilPrivate jwtTokenUtilPrivate;

	// https://www.logicbig.com/how-to/code-snippets/jcode-spring-mvc-setting-request-and-response-headers.html
	@PostMapping(produces = "text/html",
		     // consumes = "application/x-www-form-urlencoded")
		     consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	public String
	makeToken(@RequestParam Map<String,String> req,
		  Model model,
		  HttpServletResponse response,
		  @Value("${with.jwt.redirect}") String redirect) {
		String uname = req.get("username");
		// String pass = req.get("password");

		// assuming the username check was done through filters :
		String token = jwtTokenUtilPrivate .generateToken (uname);

		//response.addHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		// response.addHeader("Cache-Control","no-cache"); // overriden by no-store
		model.addAttribute("name",uname);
		model.addAttribute("token",token);
		model.addAttribute("redirect",redirect);
		return "welcome";
	}
}
