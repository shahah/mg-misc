package com.mgsoft.jwtauthserver.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@CrossOrigin
//@RestController // = @Controller + @ResponseBody // you don't want that here
@Controller
@RequestMapping({ "/login" })
public class LoginPageController {
	@GetMapping
	String loginPage() {
		return "login";
	}
}
