package com.mgsoft.jwtauthserver.controller;

import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletResponse;

import com.mgsoft.jwtauthserver.util.JwtTokenUtilPrivate;
import com.mgsoft.jwtauthserver.model.ApiUser;
import com.mgsoft.jwtauthserver.model.LoginErrors;
// import com.mgsoft.model.JwtRequest;
// import com.mgsoft.model.JwtResponse;
import com.mgsoft.jwtauthserver.service.ApiUserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


// import java.util.ArrayList;
// import java.util.List;

@CrossOrigin
// @RestController // = @Controller + @ResponseBody
@Controller
@RequestMapping({ "/change-password" })
public class ChangePassword {


	@Autowired
	LoginErrors loginErrors;

	@Autowired
	private ApiUserService aus;

	@GetMapping
	String changePassword(Model model) {
		return "change-password";
	}

	@RequestMapping(value = "/error", method = {RequestMethod.POST , RequestMethod.GET})
	String changePasswordError(Model model) {
		model.addAttribute("erreurs", loginErrors.getErrors());
		loginErrors.getUsername().map((uname) -> model.addAttribute("username", uname));

		return "change-password";
	}

	@PostMapping(produces = "text/html",
		    consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
	public String
	attemptChangePassword(
		@RequestParam (name = "username") String uname,
		@RequestParam (name = "new-password") String npass,
		@RequestParam (name = "repeat-new-password") String rnpass,
		HttpServletResponse response,
		Model model) throws UsernameNotFoundException {

		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);

		if (!(npass.equals(rnpass))) {
			String [] ms = {"password mismatch"};
			model.addAttribute("username", uname);
			model.addAttribute("erreurs", ms);
			return "change-password";
		}

		aus.updatePassword(uname, npass);
		//response.addHeader("Cache-Control","no-store");
		// response.addHeader("Cache-Control","no-cache"); // overriden by no-store

		return "password-changed";
	}
}
