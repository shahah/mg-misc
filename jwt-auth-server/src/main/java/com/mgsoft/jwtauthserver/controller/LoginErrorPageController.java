package com.mgsoft.jwtauthserver.controller;

import com.mgsoft.jwtauthserver.model.LoginErrors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@CrossOrigin
//@RestController // = @Controller + @ResponseBody // you don't want that here
@Controller
@RequestMapping({ "/login/error" })
public class LoginErrorPageController {


	@Autowired
	LoginErrors loginErrors;

	@RequestMapping(method = {RequestMethod.POST, RequestMethod.GET})
	String loginErrorPage(Model model) { // (@RequestParam Map<String, String> req, Model model) {
		model.addAttribute("erreurs",loginErrors.getErrors());
		loginErrors.getUsername().map((uname) -> model.addAttribute("username", uname));

		return "login";
	}

}
