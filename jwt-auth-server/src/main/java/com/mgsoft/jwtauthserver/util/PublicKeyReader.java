package com.mgsoft.jwtauthserver.util;

import java.io.*;
import java.security.*;
import java.security.spec.*;

import org.springframework.util.FileCopyUtils;

public class PublicKeyReader {

  public static PublicKey get(InputStream fis)
    throws Exception {

    // File f = new File(filename);
    // FileInputStream fis = new FileInputStream(f);
    // DataInputStream dis = new DataInputStream(fis);
    // byte[] keyBytes = new byte[(int)f.length()];

    byte[] keyBytes = FileCopyUtils.copyToByteArray(fis);
    // dis.readFully(keyBytes);
    // dis.close();

    X509EncodedKeySpec spec =
      new X509EncodedKeySpec(keyBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    return kf.generatePublic(spec);
  }
}
