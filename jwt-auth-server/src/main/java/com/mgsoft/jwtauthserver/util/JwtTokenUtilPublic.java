package com.mgsoft.jwtauthserver.util;

import java.io.Serializable;
// import java.security.PublicKey;
import java.util.Date;
// import java.util.HashMap;
// import java.util.Map;
import java.util.function.Function;

import com.mgsoft.jwtauthserver.spec.PublicKeyed;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
// import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtilPublic implements Serializable {

	private static final long serialVersionUID = -2550185165626007488L;
	
	public static final long JWT_TOKEN_VALIDITY = 5*60*60;
	
	// @Autowired
	// private PublicKey pub ;

	private JwtParser jwtParser ;

	@Autowired
	public JwtTokenUtilPublic(PublicKeyed pub) {
		// this .pub = pub ;
		// this .jwtParser = Jwts .parserBuilder () .setSigningKey (rsa .getKeyPair () .getPrivate ()) .build () ; // private subsumes public ?
		this .jwtParser = Jwts .parserBuilder () .setSigningKey (pub .getPublicKey()) .build () ; // but we don't need the private part
	}

	// @Value("${jwt.secret}") // from application.properties
	// private String secret;

	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject); // checks the signature
	}

	public Date getIssuedAtDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getIssuedAt); // checks the signature
	}

	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration); // checks the signature
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token); // checks the signature
		return claimsResolver.apply(claims);
	}

	private Claims getAllClaimsFromToken(String token) {
		return jwtParser .parseClaimsJws (token) .getBody (); // parseClaimsJws does the actual signature
	}

	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	private Boolean ignoreTokenExpiration(String token) {
		return false;
	}



	public Boolean canTokenBeRefreshed(String token) {
		return (!isTokenExpired(token) || ignoreTokenExpiration(token));
	}

	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token) ; // checks the signature
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}
}
