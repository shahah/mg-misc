package com.mgsoft.jwtauthserver.util;

import java.io.Serializable;
import java.security.PrivateKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
// import java.util.function.Function;

import com.mgsoft.jwtauthserver.spec.PrivateKeyed;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

// import io.jsonwebtoken.Claims;
// import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtilPrivate implements Serializable {

	private static final long serialVersionUID = -2550186007488L;

	public static final long JWT_TOKEN_VALIDITY = 5*60*60;

	// @Autowired
	private PrivateKey privy ;

	@Autowired
	public JwtTokenUtilPrivate(PrivateKeyed privy) {
		this .privy = privy .getPrivateKey() ;
		// this .jwtParser = Jwts .parserBuilder () .setSigningKey (rsa .getKeyPair () .getPrivate ()) .build () ; // private subsumes public ?
	}

	public String generateToken(String username) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims, username);
	}

	private String doGenerateToken(Map<String, Object> claims, String subject) {

		return Jwts.builder()
			.setClaims(claims)
			.setSubject(subject)
			.setIssuedAt(new Date(System.currentTimeMillis()))
			.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY*1000))
			// .signWith(SignatureAlgorithm.HS512, secret)
			.signWith(privy , SignatureAlgorithm.RS256)
			.compact() ;
	}

}
