package com.mgsoft.jwtauthserver.util ;

import java.security.KeyPair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

// import javax.crypto.SecretKey;

// import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.PrivateKey;

import com.mgsoft.jwtauthserver.spec.PrivateKeyed;

@Component
@PropertySource("classpath:values.properties")
public class PrivateFromFile implements PrivateKeyed { // 256 ??

	private PrivateKey privy ;

	// https://blog.jonm.dev/posts/rsa-public-key-cryptography-in-java/
	@Autowired
	public PrivateFromFile (@Value("${jwt.key.private}") String filePrv) {
		try {
			ResourceLoader resourceLoader = new DefaultResourceLoader();
			Resource prvKey = resourceLoader.getResource("classpath:" + filePrv);
			privy = PrivateKeyReader.get(prvKey.getInputStream());
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public PrivateKey getPrivateKey() {
		return privy;
	}

}
