package com.mgsoft.jwtauthserver.util ;

import java.security.KeyPair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

// import javax.crypto.SecretKey;

// import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

// import java.security.PrivateKey;
import java.security.PublicKey;

import com.mgsoft.jwtauthserver.spec.PublicKeyed;

@Component
@PropertySource("classpath:values.properties")
public class PublicFromFile implements PublicKeyed { // 256 ??

	private PublicKey pub ;

	// https://blog.jonm.dev/posts/rsa-public-key-cryptography-in-java/
	@Autowired
	public PublicFromFile (@Value("${jwt.key.public}") String filePub) {
		try {
			ResourceLoader resourceLoader = new DefaultResourceLoader();
			Resource pubKey = resourceLoader.getResource("classpath:" + filePub);
			pub = PublicKeyReader.get(pubKey.getInputStream());
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public PublicKey getPublicKey() {
		return pub;
	}

}
