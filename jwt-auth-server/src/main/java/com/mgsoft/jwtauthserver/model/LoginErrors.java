package com.mgsoft.jwtauthserver.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Optional;

public class LoginErrors implements Serializable {

	private static final long serialVersionUID = 234237285521L;

	private Optional<String> username = Optional.empty();
	private ArrayList<String> errors = new ArrayList<String>();

	public Optional<String> getUsername() {
		return username;
	}
	public void setUsername(Optional<String> username) {
		this.username = username;
	}
	public ArrayList<String> getErrors() {
		return errors;
	}
}
