package com.mgsoft.jwtauthserver.model;

import java.io.Serializable;

public class JwtClaims implements Serializable {

	private static final long serialVersionUID = 2342888L;

	private String username ;
	private String role ;

	public String getUsername() {
		return username;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
