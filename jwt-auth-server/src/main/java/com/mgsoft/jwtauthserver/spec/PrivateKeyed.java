package com.mgsoft.jwtauthserver.spec ;

import java.security.PrivateKey;

public interface PrivateKeyed {
	public PrivateKey getPrivateKey();
}
