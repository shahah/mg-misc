package com.mgsoft.jwtauthserver.spec ;

import java.security.PublicKey;

public interface PublicKeyed {
	public PublicKey getPublicKey();
}
