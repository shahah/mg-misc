

## Pokretanje aplikacije

definišući u resources/values.properties da po uspješnom prijavljivanju preusmjerava na adresu /login ovog frontenda pokreni (spring) auth server :

	# u folderu mg-misc/jwt-auth-server
	mvn spring-boot:run


pokreni backend/frontend server :

	# u folderu mg-misc/jwt-module
	mvn spring-boot:run





### Parametri :

`public/tokenCheck.js` sadrži adresu auth servera (za preusmjenje u slučaju da tokena nema)

### Ključevi :

generiši ili prekopiraj odnekud ključeve za potpisivanje json web tokena :

https://stackoverflow.com/questions/11410770/load-rsa-public-key-from-file

ubaci `public_key.der` u folder `mg-misc/jwt-module/src/main/resources`,

ubaci i `public_key.der` i `private_key.der` u folder `jwt-auth-server/jwt-module/src/main/resources`


## Kako radi

Neka su `FRONTEND` i `AUTH` adrese odgovarajućih servera (8080 i 8090)

1. otvaranjem `FRONTEND/` adrese provjerava se ima li tokena, ako ga nema, preusmjeri na `AUTH/login`
2. unese se na `AUTH/login` username i password ('mgsoft' i 'password')
   i ide POST te forme na adresu `AUTH/welcome`
3. ako su validni podaci, `AUTH/welcome` izdaje jwt i POST-uje ga na adresu `FRONTEND/store-token`
4. TODO : `FRONTEND/store-token` provjerava validnost primljenog jwt-a ?
5. `FRONTEND/store-token` upisuje taj jwt u `sessionStorage` i predaje kontrolu js aplikaciji
   preusmjeravajući na `FRONTEND/`
6. sada js aplikacija može da pravi upite na adrese `FRONTEND/secure/**` a da ne dobija 401 Unauthorized

pripremljena je i stranica `FRONTEND/index.html` na kojoj može da se isproba radi li
