package com.techgeeknext.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.AnyRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.context.annotation.RequestScope;


// https://docs.spring.io/spring-security/site/docs/current/reference/html5/#multiple-httpsecurity
// @Configuration // superfluous , implied by @EnableWebSecurity
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

	

	private static final RequestMatcher PROTECTED_URLS =
		new NegatedRequestMatcher (
			new OrRequestMatcher(
				new AntPathRequestMatcher ("/authenticate"),
				AnyRequestMatcher.INSTANCE )) ;
		// new AndRequestMatcher(new NegatedRequestMatcher( new AntPathRequestMatcher("/authenticate")), new AntPathRequestMatcher("/**"));
	//private static final RequestMatcher AUTH_URL =
	//	new OrRequestMatcher(new AntPathRequestMatcher("/authenticate"));

	@Configuration
	@Order(1)
	public static class NoAuthConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			// We don't need CSRF for this example
			httpSecurity
				.csrf().disable()
				.authorizeRequests()
				////.requestMatchers(AUTH_URL).permitAll()
				.requestMatchers(new NegatedRequestMatcher(PROTECTED_URLS)).permitAll()
				//.antMatchers("/authenticate").permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		
		}
		
	} 


	@Configuration // no @Order means last
	public static class AuthConfig extends WebSecurityConfigurerAdapter {
		
		@Autowired
		private UserDetailsService jwtUserDetailsService;
		
		@Bean
		public PasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder(); // BCrypt hash
		}

		@Bean
		public PreAuthProvider preAuthProvider () {
			return new PreAuthProvider();
		}
		
		// autowired method, called on bean instantiation, with injected args, after class field injection
		@Autowired
		public void configureGlobal(AuthenticationManagerBuilder authBuild) throws Exception {
			// configure AuthenticationManager so that it knows from where to load
			// user for matching credentials
			// Use BCryptPasswordEncoder
			authBuild.authenticationProvider(preAuthProvider());
			authBuild
				.userDetailsService (jwtUserDetailsService) // THE SERVICE GOES HERE
				.passwordEncoder (passwordEncoder()) ;
		}

		@Autowired
		private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

		@Bean
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

		@Bean
		JwtAuthFilter jwtAuthFilter() throws Exception {
			final JwtAuthFilter filter = new JwtAuthFilter(PROTECTED_URLS);
			filter.setAuthenticationManager(authenticationManager());
			//filter.setAuthenticationSuccessHandler(successHandler());
			return filter;
		}

		// @Autowired
		// private JwtRequestFilter jwtRequestFilter;
		
		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			// We don't need CSRF for this example
			httpSecurity
				.csrf().disable()
				.authorizeRequests()
				//.antMatchers("/**").authenticated()
				.requestMatchers(PROTECTED_URLS).authenticated()
				//.anyRequest().authenticated()
				// make sure we use stateless session; session won't be used to
				// store user's state.
				.and()
				// no auth happens without these two lines :
				.exceptionHandling()
				.authenticationEntryPoint(jwtAuthenticationEntryPoint)
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.addFilterBefore(jwtAuthFilter(), AnonymousAuthenticationFilter.class); // UsernamePasswordAuthenticationFilter.class) ;
				//.addFilterBefore(jwtRequestFilter, AnonymousAuthenticationFilter.class);
			// so security merely forces you to have an auth header
			// the filters do the auth checking ? (?)
		}
		
	} 
	

	@Bean
	@RequestScope
	public JwtData jwtData() {
		return new JwtData();
	}



}
