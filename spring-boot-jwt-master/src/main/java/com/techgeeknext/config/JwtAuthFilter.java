package com.techgeeknext.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.techgeeknext.util.JwtTokenUtilPublic;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.util.matcher.RequestMatcher;
// import org.springframework.stereotype.Component;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.security.SignatureException;


// @Component
public class JwtAuthFilter extends AbstractAuthenticationProcessingFilter {

	protected JwtAuthFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
		super(requiresAuthenticationRequestMatcher);
		//setAuthenticationManager(new NoOpAuthenticationManager()); // ?
		// Auto-generated constructor stub
	}

	@Autowired
	private JwtTokenUtilPublic jwtTokenUtil;

	@Autowired
	private JwtData jwtData ; // bean def in WebSecurityConfig

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException, IOException, ServletException {

		final String requestTokenHeader = request.getHeader("Authorization") ;
		String username = null ;
		String jwtToken = null ;

		// JWT Token is in the form "Bearer <token>"
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader .substring (7); // strip "Bearer "
			try {
				// check the signature (may throw) :
				username = jwtTokenUtil.getUsernameFromToken(jwtToken) ;
				logger.warn("JWT valid");

				jwtData .setUsername (username) ;

				// issue the All Powerful PreAuthenticated token :
				PreAuthenticatedAuthenticationToken authToken = new PreAuthenticatedAuthenticationToken(username, "") ;

				authToken.setDetails(jwtToken); // relevant how ?

				Authentication auth = getAuthenticationManager () .authenticate (authToken) ;
				logger.warn("JWT affirmed by auth manager");

				return auth;
			} catch (IllegalArgumentException e) {
				System.out.println("JWT ineffable");
			} catch (ExpiredJwtException e) {
				System.out.println("JWT is no more");
				// System.out.println("JWT has ceased to be");
			} catch (SignatureException e) {
				System.out.println("JWT signature doesn't check out") ;
			}
		} else {
			logger.warn("JWT does not begin with Bearer String");
		}

		logger.warn("WAT");
		return null;
	}

	@Override
	protected void successfulAuthentication(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain, final Authentication authResult) throws IOException, ServletException {
		SecurityContextHolder.getContext().setAuthentication(authResult);
		logger.warn("Auth successful");
		chain.doFilter(request, response);
	}

}
