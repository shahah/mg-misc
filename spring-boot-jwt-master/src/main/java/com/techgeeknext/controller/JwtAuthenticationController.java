package com.techgeeknext.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.techgeeknext.util.JwtTokenUtilPrivate;
import com.techgeeknext.model.JwtRequest;
import com.techgeeknext.model.JwtResponse;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	// AuthenticationManager being used outside filters
	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtilPrivate jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> generateAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
			throws Exception {

		checkAuth (authenticationRequest .getUsername (), authenticationRequest .getPassword ());
		// double fetch from service, once in checkAuth, then below
		final UserDetails userDetails = userDetailsService .loadUserByUsername (authenticationRequest .getUsername ());

		final String token = jwtTokenUtil .generateToken (userDetails);

		return ResponseEntity .ok (new JwtResponse(token));
	}

	private void checkAuth (String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);
		try {	// fetch user by `username' from userDetailsService
			// (because of config in WebSecurityConfig.java)
			// then compare its hash
			// with the hash of this here `password'
			// also discard the resulting authorities object (wat)
			authenticationManager .authenticate (new UsernamePasswordAuthenticationToken (username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
