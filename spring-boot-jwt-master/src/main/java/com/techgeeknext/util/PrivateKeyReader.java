package com.techgeeknext.util ;

import java.io.*;
import java.security.*;
import java.security.spec.*;

import org.springframework.util.FileCopyUtils;

public class PrivateKeyReader {

  public static PrivateKey get(InputStream fis)
    throws Exception {
    
    // File f = new File(filename);
    // FileInputStream fis = new FileInputStream(f);
    //DataInputStream dis = new DataInputStream(fis);

    byte[] keyBytes = FileCopyUtils.copyToByteArray(fis);
    // dis.readFully(keyBytes);
    // dis.close();

    PKCS8EncodedKeySpec spec =
      new PKCS8EncodedKeySpec(keyBytes);
    KeyFactory kf = KeyFactory.getInstance("RSA");
    return kf.generatePrivate(spec);
  }
}
