package com.mgsoft.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mgsoft.model.JwtData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

public class LoginAuthFilter extends UsernamePasswordAuthenticationFilter {

	protected LoginAuthFilter(RequestMatcher requiresAuthenticationRequestMatcher) {
		super () ;
		this.setRequiresAuthenticationRequestMatcher (requiresAuthenticationRequestMatcher) ;
		// matches form names in LoginPage.java
		this.setUsernameParameter("username");
		this.setPasswordParameter("password");
	}

	@Autowired
	private JwtData jwtData ; // bean def in WebSecurityConfig

	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
		jwtData.setUsername(this.obtainUsername(request)) ;
		logger.warn("trying to auth");
		// the previously 'filter.setAuthenticationSuccessHandler(ash)'
		// decides the redirect that follows ? not sure anymore :
		return super.attemptAuthentication(request, response);
	}

}
