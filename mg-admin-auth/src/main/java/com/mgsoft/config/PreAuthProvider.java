package com.mgsoft.config;

import org.springframework.stereotype.Component;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;


public class PreAuthProvider implements AuthenticationProvider {
	@Override
	public boolean supports(Class<?> auth) {
	        return auth.equals(PreAuthenticatedAuthenticationToken.class);
	}

	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		return auth; // let 'em through, no questions asked
	}
}
