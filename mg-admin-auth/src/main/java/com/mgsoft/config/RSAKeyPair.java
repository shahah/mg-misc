package com.mgsoft.config ;

import java.io.InputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

// import javax.crypto.SecretKey;

// import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

// import io.jsonwebtoken.SignatureAlgorithm;
// import io.jsonwebtoken.security.Keys;

@Component
@PropertySource("classpath:values.properties")
public class RSAKeyPair {

	private KeyPair keyPair ;
	
//	Jwts .parserBuilder () .setSigningKey (rsa .getKeyPair () .getPrivate ())
//			.build ()
	
	// public RS256 () {
	// 	keyPair = Keys .keyPairFor (SignatureAlgorithm.RS512) ;
	// }

	@Autowired
	public RSAKeyPair (//@Value("${jwt.key.algorithm}") String alg,
			     @Value("${jwt.key.private}") String filePrv,
			     @Value("${jwt.key.public}") String filePub) {
		try {
			ResourceLoader resourceLoader = new DefaultResourceLoader();
			Resource prvKey = resourceLoader.getResource("classpath:" + filePrv);
			Resource pubKey = resourceLoader.getResource("classpath:" + filePub);
			PrivateKey privateKey = getPrivate(prvKey.getInputStream());
			PublicKey publicKey = getPublic(pubKey.getInputStream());
			keyPair = new KeyPair(publicKey, privateKey);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	public KeyPair getKeyPair() {
		return keyPair;
	}
	public static PrivateKey getPrivate(InputStream fis)
		throws Exception {
		byte[] keyBytes = FileCopyUtils.copyToByteArray(fis);
		PKCS8EncodedKeySpec spec =
			new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}
	public static PublicKey getPublic(InputStream fis)
		throws Exception {
		byte[] keyBytes = FileCopyUtils.copyToByteArray(fis);
		// PKCS8EncodedKeySpec spec =
		//	new PKCS8EncodedKeySpec(keyBytes);
		X509EncodedKeySpec spec =
			new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}
}
