package com.mgsoft.config;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class JwtTokenUtil implements Serializable {

	private static final long serialVersionUID = -2550185165626007488L;
	
	public static final long JWT_TOKEN_VALIDITY = 5*60*60;
	
	private RSAKeyPair rsa ;

	private JwtParser jwtParser ;

	// autowired ???
	public JwtTokenUtil(RSAKeyPair rsa) {
		this .rsa = rsa ;
		this .jwtParser = Jwts .parserBuilder () .setSigningKey (rsa .getKeyPair () .getPrivate ()) .build () ;
	}

	// @Value("${jwt.secret}") // from application.properties
	// private String secret;

	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject); // checks the signature
	}

	public Date getIssuedAtDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getIssuedAt); // checks the signature
	}

	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration); // checks the signature
	}

	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token); // checks the signature
		return claimsResolver.apply(claims);
	}

	private Claims getAllClaimsFromToken(String token) {
		return jwtParser .parseClaimsJws (token) .getBody (); // parseClaimsJws does the actual signature
	}

	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	private Boolean ignoreTokenExpiration(String token) {
		return false;
	}

	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims, userDetails.getUsername());
	}

	private String doGenerateToken(Map<String, Object> claims, String subject) {

		return Jwts.builder()
			.setClaims(claims)
			.setSubject(subject)
			.setIssuedAt(new Date(System.currentTimeMillis()))
			.setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY*1000))
			// .signWith(SignatureAlgorithm.HS512, secret)
			.signWith(rsa .getKeyPair() .getPrivate () , SignatureAlgorithm.RS256)
			.compact() ;
	}

	public Boolean canTokenBeRefreshed(String token) {
		return (!isTokenExpired(token) || ignoreTokenExpiration(token));
	}

	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token) ; // checks the signature
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}
}
