package com.mgsoft.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.util.matcher.AndRequestMatcher;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
// import org.springframework.security.web.util.matcher.AndRequestMatcher;
// import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.AnyRequestMatcher;
import org.springframework.security.web.util.matcher.NegatedRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.context.annotation.RequestScope;
import com.mgsoft.model.JwtData;


// https://docs.spring.io/spring-security/site/docs/current/reference/html5/#multiple-httpsecurity
// @Configuration // superfluous , implied by @EnableWebSecurity
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {


	private static final RequestMatcher UNAME_PASS_PROTECTED_URLS =
		new AndRequestMatcher(
			new AntPathRequestMatcher("/welcome"),
			new AntPathRequestMatcher("/login"));

	private static final RequestMatcher JWT_PROTECTED_URLS =
		new NegatedRequestMatcher (
			new OrRequestMatcher(
				UNAME_PASS_PROTECTED_URLS,
				new AntPathRequestMatcher("/login"),
				new AntPathRequestMatcher ("/authenticate"),
				AnyRequestMatcher.INSTANCE )) ;
	//private static final RequestMatcher AUTH_URL =
	//	new OrRequestMatcher(new AntPathRequestMatcher("/authenticate"));

	@Bean
	@RequestScope
	public JwtData jwtData() {
		return new JwtData();
	}


	@Configuration
	@Order(1)
	public static class NoAuthConfig extends WebSecurityConfigurerAdapter {

		// does this need a declared auth manager ?

		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			httpSecurity
				.csrf().disable() // no cookies
				.authorizeRequests()
				////.requestMatchers(AUTH_URL).permitAll()
				.requestMatchers(new NegatedRequestMatcher(JWT_PROTECTED_URLS)).permitAll()
				//.antMatchers("/authenticate").permitAll()
				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		}
	}

	@Configuration
	@Order(2)
	public static class UsernamePasswordAuthConfig extends WebSecurityConfigurerAdapter {


		@Bean(name="unamePassAM")
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}


		@Bean
		LoginAuthFilter loginAuthFilter() throws Exception {
			final LoginAuthFilter filter = new LoginAuthFilter(UNAME_PASS_PROTECTED_URLS);
			filter.setAuthenticationManager(authenticationManager());
			// filter.setAuthenticationFailureHandler(failureHandler());
			// SavedRequestAwareAuthenticationSuccessHandler ash = new SavedRequestAwareAuthenticationSuccessHandler() ;
			// // defaults to '/login' ? :
			// ash.setTargetUrlParameter("/welcome");
			// filter.setAuthenticationSuccessHandler(ash);
			return filter;
		}

		@Autowired
		private UserDetailsService uds ;

		// spring magic rewrites this method ?
		// so that it makes only one PasswordEncoder
		@Bean
		public PasswordEncoder passwordEncoder() {
			return new BCryptPasswordEncoder(); // BCrypt hash
		}


		@Override
		protected void configure(AuthenticationManagerBuilder amb) throws Exception {
			amb.userDetailsService(uds).passwordEncoder(passwordEncoder());
		}

		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			// We don't need CSRF for this example
			httpSecurity
				.csrf().disable() // no cookies
				.authorizeRequests()
				////.requestMatchers(AUTH_URL).permitAll()
				.requestMatchers(UNAME_PASS_PROTECTED_URLS).authenticated()
				//.antMatchers("/authenticate").permitAll()
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.addFilterBefore(loginAuthFilter(), AnonymousAuthenticationFilter.class);

		}
	}


	// TODO : delete this ? does an auth provider app ever need to verify tokens on endpoints ?
	@Configuration // no @Order means that it goes LAST
	public static class JwtAuthConfig extends WebSecurityConfigurerAdapter {


		@Bean
		public PreAuthProvider preAuthProvider () {
			return new PreAuthProvider();
		}

		@Autowired
		private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

		@Bean(name="jwtAM")
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}

		@Bean
		JwtAuthFilter jwtAuthFilter() throws Exception {
			final JwtAuthFilter filter = new JwtAuthFilter(JWT_PROTECTED_URLS);
			filter.setAuthenticationManager(authenticationManager());
			// filter.setAuthenticationSuccessHandler(successHandler()); // ??
			return filter;
		}

		// @Autowired
		// private JwtRequestFilter jwtRequestFilter;

		@Override
		protected void configure(HttpSecurity httpSecurity) throws Exception {
			// We don't need CSRF for this example
			httpSecurity
				.csrf().disable()
				.authorizeRequests()
				//.antMatchers("/**").authenticated()
				.requestMatchers(JWT_PROTECTED_URLS).authenticated()
				//.anyRequest().authenticated()
				// make sure we use stateless session; session won't be used to
				// store user's state.
				.and()
				// no auth happens without these two lines :
				.exceptionHandling()
				.authenticationEntryPoint(jwtAuthenticationEntryPoint)
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.addFilterBefore(jwtAuthFilter(), AnonymousAuthenticationFilter.class); // UsernamePasswordAuthenticationFilter.class) ;
				//.addFilterBefore(jwtRequestFilter, AnonymousAuthenticationFilter.class);
			// so security merely forces you to have an auth header
			// the filters do the auth checking ? (?)
		}

	}
}
