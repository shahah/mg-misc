package com.mgsoft.model;

// import org.springframework.context.annotation.Scope;
// import org.springframework.stereotype.Component;

// @Component // clashes with bean in config
// @Scope("request") // IllegalState ... no thread bound request, instead use bean def in config
public class JwtData {
	private String username ;
	private String role ;
	
	public String getUsername() {
		return username;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
}
