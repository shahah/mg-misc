package com.mgsoft.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class ApiUser {
    // @Id
    // @Column
    // private int id;

    @Id
    @Column
    private String username ;

    @Column
    private String bcryptHash ;

    // public int getId() {
    // 	return id;
    // }
    public String getUsername() {
	return username;
    }
    public String getBcryptHash() {
	return bcryptHash;
    }

    // public void setId(int id) {
    // 	this.id = id;
    // }
    public void setUsername(String username) {
	this.username = username;
    }
    public void setBcryptHash(String bcryptHash) {
	this.bcryptHash = bcryptHash;
    }
}
