package com.mgsoft.model;

import java.io.Serializable;


// mere @RequestMapping scaffolding to be converted to json
// http response body json has the field .tokenus , not .jwttoken
// so the field name is based on the getter name
public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String jwttoken;

	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getTokenus() {
		return this.jwttoken;
	}
}
