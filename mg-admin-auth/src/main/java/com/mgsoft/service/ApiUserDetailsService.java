package com.mgsoft.service;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import com.mgsoft.model.ApiUser;
import com.mgsoft.repository.ApiUserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

// used in WebSecurityConfig.java in configureGlobal
@Primary
@Service
public class ApiUserDetailsService implements UserDetailsService {

    @Autowired
    ApiUserRepository apiUserRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
	
	try {
	    // //final ApiUser apiUser = apiUserRepository.findOptionalByUsername(username).get();
	    final ApiUser apiUser = apiUserRepository.findById(username).get();
	    return new User(username, apiUser.getBcryptHash(), new ArrayList<>());
	    //return new User("mgsoft", "$2a$10$ixlPY3AAd4ty1l6E2IsQ9OFZi2ba9ZQE0bP7RFcGIWNhyFrrT3YUi",new ArrayList<>());
	} catch (final NoSuchElementException e) {
	    throw new UsernameNotFoundException("No such user : " + username);
	}
    }
}
