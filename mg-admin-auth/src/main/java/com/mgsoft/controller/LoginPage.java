package com.mgsoft.controller;

import com.mgsoft.model.ApiUser;
// import com.mgsoft.model.Employee;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// import java.util.ArrayList;
// import java.util.List;

@CrossOrigin
@RestController
@RequestMapping({ "/login" })
public class LoginPage {


	// @PostMapping(produces = "text/plain",consumes = "application/x-www-form-urlencoded")
	// ResponseEntity<String> customHeader(ApiUser apiUser) { // TODO
	// 	HttpHeaders headers = new HttpHeaders();
	// 	headers .add ("Custom-Header", "foo") ;
		
	// 	return new ResponseEntity
	// 		<> ("Custom header set" + apiUser.getUsername(), headers, HttpStatus.OK);
	// }
	@GetMapping(produces = "text/html")
	ResponseEntity<String> loginForm() { // TODO : thymeleaf rewrite
		HttpHeaders headers = new HttpHeaders();
		headers .add ("Content-Type", "text/html") ;
		return new ResponseEntity <>
			( "<form action='/welcome' method='post'>"
//+ "  <div class='imgcontainer'>"
//+ "    <img src='img_avatar2.png' alt='Avatar' class='avatar'>"
//+ "  </div>"
//+ "  <div class='container'>"
			  + "    <label for='username'><b>Username</b></label>"
			  + "    <input type='text' placeholder='Enter Username' name='username' required>"
			  + "    <label for='password'><b>Password</b></label>"
			  + "    <input type='password' placeholder='Enter Password' name='password' required>"
			  + "    <button type='submit'>Login</button>"
			  + "    <label>"
			  + "      <input type='checkbox' checked='checked' name='remember'> Remember me"
			  + "    </label>"
//+ "  </div>"
			  + "  <div class='container' style='background-color:#f1f1f1'>"
			  + "    <button type='button' class='cancelbtn'>Cancel</button>"
			  + "    <span class='psw'>Forgot <a href='#'>password?</a></span>"
			  + "  </div>"
			  + "</form>"
			  , headers
			  , HttpStatus.OK);
	}

	// @GetMapping
	// public List<Employee> getMyEmployees {}


}
