package com.mgsoft.controller;

import com.mgsoft.model.JwtData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin()
// TODO : delete
public class EmployeeController {

	@Autowired
	private JwtData jwtData ; // earlier prepared by JwtAuthFilter
	
	@RequestMapping(value = "/greeting", method = RequestMethod.GET)
	public String getEmployees() {
		return "Welcome!" + jwtData .getUsername ();
	}
}
