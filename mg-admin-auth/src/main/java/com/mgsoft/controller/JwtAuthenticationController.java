package com.mgsoft.controller;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mgsoft.config.JwtTokenUtil;
import com.mgsoft.model.JwtRequest;
import com.mgsoft.model.JwtResponse;

@RestController
@CrossOrigin
public class JwtAuthenticationController {

	// TODO : rm -rf
	@Autowired
	@Qualifier("jwtAM")
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private UserDetailsService userDetailsService;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> generateAuthenticationToken(@RequestBody JwtRequest authenticationRequest)
			throws Exception {

		authenticate (authenticationRequest .getUsername (), authenticationRequest .getPassword ());
		// double fetch from service, once in authenticate, then below
		final UserDetails userDetails = userDetailsService
				.loadUserByUsername (authenticationRequest .getUsername ());

		final String token = jwtTokenUtil .generateToken (userDetails);

		return ResponseEntity .ok (new JwtResponse(token));
	}

	private void authenticate(String username, String password) throws Exception {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);
		try {	// fetch user by `username' from jwtUserDetailsService
			// (because of config in WebSecurityConfig.java)
			// then compare its hash
			// with the hash of this here `password'
			// also discard the resulting authorities object (wat)
			authenticationManager
				.authenticate (new UsernamePasswordAuthenticationToken (username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}
}
