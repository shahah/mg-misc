


INSERT INTO api_user (username,bcrypt_hash)
VALUES ('megasoft' ,'$2a$10$dmISGK0VnIx3oGz0awW8ieS8X5O3XmtMwkg.zODnI0CyfijXHO3Mm');
-- VALUES ('megasoft' ,'$2y$12$WGt1vzHkJWzz5VEpmEgrYO/ZB23EikcIhQ9pQBH2uQH9F5Cr2jTVq');
-- cryptic_hash

INSERT INTO api_user (username,bcrypt_hash)
VALUES ('macrohard','$2y$12$VfuUvPGNhRGmf0JQaU0XleMP8AOAPCzIpWOsPQsbZwxfc0YDCJpWG');
-- troublesome-double

INSERT INTO api_user (username,bcrypt_hash)
VALUES ('mgsoft','$2a$10$ixlPY3AAd4ty1l6E2IsQ9OFZi2ba9ZQE0bP7RFcGIWNhyFrrT3YUi');
-- password


-- ?
-- COMMIT ;
